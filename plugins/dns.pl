#!/usr/bin/perl

use strict;
use warnings;

# use module
use Data::Dumper;
use DBI;
use DateTime;

#
my $dbh;
my %db_config;
$db_config{'dbdriver'} = "mysql";
$db_config{'dbname'}   = "racktables_db";
$db_config{'dbserver'} = "localhost";
$db_config{'dbuser'}   = "racktables_user";
$db_config{'dbpass'}   = "racktables_password";

 $dbh = db_connect(\%db_config);
 my $query;
 my $sth;
 my %myrow;
 my $ref;

$query = q{
SELECT
        INET_NTOA( IPv4Network.ip ) AS mynet ,
        INET_NTOA( IPv4Allocation.ip )AS myip ,
        IPv4Allocation.ip AS rawip ,
        IPv4Network.name AS portname,
        IPv4Address.name AS currentname,
        TRIM(
            SUBSTRING(IPv4Network.comment,
                LOCATE('DOMAIN:', IPv4Network.comment) + LENGTH('DOMAIN:'),
                LENGTH(SUBSTRING(IPv4Network.comment,
                        LOCATE('DOMAIN:',CONCAT(IPv4Network.comment,'\n')
                        )+LENGTH('DOMAIN:'),
                        LOCATE('\n',CONCAT(IPv4Network.comment,'\n'),LOCATE('DOMAIN:', IPv4Network.comment))
                        - (LOCATE('DOMAIN:',IPv4Network.comment) + LENGTH('DOMAIN:'))
                )
                )
            )
        ) AS thedomain,
        SUBSTRING(Object.name, 1, LOCATE('.',CONCAT(Object.name,'.')) - 1) AS hostname,
	AttributeValue.string_value as fqdn
        from IPv4Allocation,  IPv4Network, Object , IPv4Address, AttributeValue
        WHERE Object.id = IPv4Allocation.object_id 
	AND  Object.id = AttributeValue.object_id
	AND  AttributeValue.attr_id = 3
        AND  IPv4Address.ip = IPv4Allocation.ip 
        AND  IPv4Allocation.ip  & ((4294967295 << (32 - mask)) & 4294967295) = IPv4Network.ip
        AND IPv4Network.comment REGEXP 'DOMAIN:' ;

};
        if( !($sth = $dbh->prepare($query)) ) {
                print STDERR "LOG" .  "Can't prepare $query: Reason $!";
                exit;
        } elsif ( !$sth->execute() ) {
                print STDERR "LOG" .  "Can't execute $query: Reason $!";
                exit;
        }
        while($ref = $sth->fetchrow_hashref) {
		%myrow = %$ref;
		print "# ";
                foreach my $key ( keys(%myrow)){
                	print "$key=$myrow{$key},";
                }
#rawip=174326025,myip=10.100.1.9,currentname=33,thedomain=web.sycle.net,portname=web-app-common,hostname=ws9,mynet=10.100.1.0,
		print "\n# fqdn = $myrow{'fqdn'}\n";
                print "\nINSERT INTO IPv4Address (ip,name,comment,reserved) VALUES($myrow{'rawip'},'$myrow{'hostname'}','dnsupdate','') ON DUPLICATE KEY UPDATE name = '$myrow{'hostname'}.$myrow{'thedomain'}' ;\n";
        }



db_disconnect($dbh);
exit;


###########################################################################
## Connect to the database
############################################################################

sub db_connect{
   my $config = shift;
     my $DSN = "dbi:$config->{'dbdriver'}:database=$config->{'dbname'};host=$config->{'dbserver'}";
       my $dbh = DBI->connect($DSN,$config->{'dbuser'},$config->{'dbpass'}) or die "Cannot connect to '$DSN'";
         return $dbh;
}

###########################################################################
# Disconnects from the database
###########################################################################
sub db_disconnect{
    my $dbh  = shift;
      defined $dbh and
        ($dbh->disconnect or die "Can't disconnect from database. Reason: $DBI::errstr");
}


