#!/usr/bin/perl

use strict;
use warnings;

# use module
use Data::Dumper;
use DBI;
use DateTime;

#
my $dbh;
my %db_config;
$db_config{'dbdriver'} = "mysql";
$db_config{'dbname'}   = "racktables_db";
$db_config{'dbserver'} = "localhost";
$db_config{'dbuser'}   = "racktables_user";
$db_config{'dbpass'}   = "racktables_password";

 $dbh = db_connect(\%db_config);
 my $query;
 my $sth;
 my %myrow;
 my $ref;

$query = "
SELECT
        INET_NTOA( IPv4Network.ip ) AS mynet ,
	IPv4Address.ip AS rawip ,
        INET_NTOA( IPv4Address.ip )AS myip ,
        IPv4Network.name AS portname,
        IPv4Address.name AS currentname,
        TRIM(
            SUBSTRING(IPv4Network.comment,
                LOCATE('DOMAIN:', IPv4Network.comment) + LENGTH('DOMAIN:'),
                LENGTH(SUBSTRING(IPv4Network.comment,
                        LOCATE('DOMAIN:',CONCAT(IPv4Network.comment,'\n')
                        )+LENGTH('DOMAIN:'),
                        LOCATE('\n',CONCAT(IPv4Network.comment,'\n'),LOCATE('DOMAIN:', IPv4Network.comment))
                        - (LOCATE('DOMAIN:',IPv4Network.comment) + LENGTH('DOMAIN:'))
                )
                )
            )
        ) AS thedomain
        from  IPv4Network,  IPv4Address
        WHERE  IPv4Address.ip  & ((4294967295 << (32 - mask)) & 4294967295) = IPv4Network.ip
        AND IPv4Network.comment REGEXP 'DOMAIN:' ORDER BY thedomain;
";
        if( !($sth = $dbh->prepare($query)) ) {
                print STDERR "LOG" .  "Can't prepare $query: Reason $!";
                exit;
        } elsif ( !$sth->execute() ) {
                print STDERR "LOG" .  "Can't execute $query: Reason $!";
                exit;
        }
        while($ref = $sth->fetchrow_hashref) {
		%myrow = %$ref;
                #foreach my $key ( keys(%myrow)){
                	#print "$key=$myrow{$key},";
                #}
		#print "\n";
		my ($oct4,$oct3,$oct2,$oct1) = split(/\./,$myrow{'myip'});
		
#rawip=174326270,myip=10.100.1.254,currentname=wslb1-rh6.web.sycle.net,thedomain=web.sycle.net,portname=web-app-common,mynet=10.100.1.0,
              open(OUTFILE, '>>', "/tmp/$myrow{'thedomain'}.txt");
	      print OUTFILE "$myrow{'currentname'}.		IN A $myrow{'myip'}\n";
              close(OUTFILE);

              open(OUTFILE, '>>', "/tmp/$oct2.$oct3.$oct4.in-addr-arpa.txt");
	      print OUTFILE "$oct1		IN PTR $myrow{'currentname'}.\n";
              close(OUTFILE);



        }



db_disconnect($dbh);
exit;


###########################################################################
## Connect to the database
############################################################################

sub db_connect{
   my $config = shift;
     my $DSN = "dbi:$config->{'dbdriver'}:database=$config->{'dbname'};host=$config->{'dbserver'}";
       my $dbh = DBI->connect($DSN,$config->{'dbuser'},$config->{'dbpass'}) or die "Cannot connect to '$DSN'";
         return $dbh;
}

###########################################################################
# Disconnects from the database
###########################################################################
sub db_disconnect{
    my $dbh  = shift;
      defined $dbh and
        ($dbh->disconnect or die "Can't disconnect from database. Reason: $DBI::errstr");
}


